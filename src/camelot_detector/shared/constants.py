"""Misc constants"""

from datetime import timedelta

CONTAINER_EXPIRY_TIMEDELTA = timedelta(days=40)
CONTAINER_START_LENIENCE_TIMEDELTA = timedelta(minutes=5)
JOB_OWNERSHIP_GRACE_PERIOD = timedelta(hours=120)
