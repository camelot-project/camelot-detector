"""System component configuration"""

from camelot_detector.component.log import Logger
from camelot_detector.component.storage import Storage
from camelot_detector.component.account import Account
from camelot_detector.component.database import Database
from camelot_detector.component.mail import SendMail
from camelot_detector.component.task import Task
from camelot_detector.component.learn import Learn
from camelot_detector.component.job import Job
from camelot_detector.component.event_bus import EventBus
from camelot_detector.component.task_queue import TaskQueue
from camelot_detector.component.clean_up_scheduler import CleanUpScheduler
import asyncio

def default_system(app):
    logger = Logger(app)
    database = Database(Logger=logger)
    sendmail = SendMail(Logger=logger)
    event_bus = EventBus(Logger=logger)
    storage = Storage(Logger=logger)

    account = Account(Logger=logger, Database=database, SendMail=sendmail)
    task_queue = TaskQueue(Logger=logger)
    task = Task(Logger=logger, Database=database, EventBus=event_bus, Storage=storage, TaskQueue=task_queue)
    clean_up_scheduler=CleanUpScheduler(Logger=logger, Task=task, TaskQueue=task_queue)

    return {
        'log': logger,
        'sendmail': sendmail,
        'event_bus': event_bus,
        'storage': storage,
        'learn': Learn(Logger=logger),
        'account': account,
        'task': task,
        'task_queue': task_queue,
        'clean_up_scheduler': clean_up_scheduler
    }
