"""Trivial Synchronous EventBus"""

from bson.objectid import ObjectId

class EventBus:
    """Trivial synchronous event bus."""

    def __init__(self, Logger):
        self.log = Logger
        self.subscribers = {}

    def subscribe(self, event_type, callback):
        if event_type in self.subscribers:
            self.subscribers[event_type].append(callback)
        else:
            self.subscribers[event_type] = [callback]

    def publish(self, event):
        if "type" not in event:
            return
        if event["type"] not in self.subscribers:
            return
        for sub in self.subscribers[event["type"]]:
            try:
                sub(event)
            except Exception as e:
                self.log.warn("event_bus", "Error while processing event %s: %s" % (event["type"], e))
