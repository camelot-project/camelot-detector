"""Manage access to storage accounts"""

from camelot_detector.shared.system import get_component
import camelot_detector.shared.constants as constants
from datetime import datetime
from azure.core.exceptions import ResourceNotFoundError
from azure.storage.blob import (
    ContainerClient,
    ContainerSasPermissions,
    generate_container_sas
)

def _build_account_url(account_name):
    return "https://%s.blob.core.windows.net" % (account_name)

def _build_container_sas_url(account_url, container_name, sas):
    return "%s/%s?%s" % (account_url, container_name, sas)

def _container_exists(service):
    try:
        service.get_container_properties()
        return True
    except ResourceNotFoundError:
        return False

def _build_service(account_url, account_key, container_name):
    return ContainerClient(
        account_url=account_url,
        container_name=container_name,
        credential=account_key
    )

def create_container(account_name, account_key, container_name):
    account_url = _build_account_url(account_name)
    service = _build_service(account_url, account_key, container_name)
    if _container_exists(service):
        raise RuntimeError('Container name collision')
    service.create_container()

def _create_container_sas(account_name, account_key, container_name, permission):
    account_url = _build_account_url(account_name)
    service = _build_service(account_url, account_key, container_name)
    sas = generate_container_sas(
        account_name,
        container_name,
        account_key=account_key,
        permission = permission,
        start=datetime.utcnow() - constants.CONTAINER_START_LENIENCE_TIMEDELTA,
        expiry=datetime.utcnow() + constants.CONTAINER_EXPIRY_TIMEDELTA
    )
    return _build_container_sas_url(account_url, container_name, sas)

def create_container_readwrite_sas(account_name, account_key, container_name):
    permission = ContainerSasPermissions(read=True, write=True, list=True, delete=True)
    return _create_container_sas(account_name, account_key, container_name, permission)

def create_container_readonly_sas(account_name, account_key, container_name):
    permission = ContainerSasPermissions(read=True, list=True)
    return _create_container_sas(account_name, account_key, container_name, permission)

def delete_container(account_name, account_key, container_name):
    account_url = _build_account_url(account_name)
    service = _build_service(account_url, account_key, container_name)
    if not _container_exists(service):
        get_component('log').warn("TaskContainer", "Container deletion failed for container: %s" % container_name)
        get_component('log').event({
            'state': 'warning',
            'service': 'detector storageaccount container delete failed count',
            'metric': 1,
            'tags': ['detector', 'storageaccount', 'container', 'delete', 'failed', 'count']
        })
    else:
        get_component('log').event({
            'state': 'ok',
            'service': 'detector storageaccount container deleted count',
            'metric': 1,
            'tags': ['detector', 'storageaccount', 'container', 'delete', 'count']
        })
        service.delete_container()
