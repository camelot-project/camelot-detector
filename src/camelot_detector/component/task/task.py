"""Task management"""

from camelot_detector.shared.http_utils import make_get_request
from bson.objectid import ObjectId
from os import environ
from bson.json_util import dumps
from camelot_detector.shared.system import get_component
from pymongo import ReturnDocument
from datetime import datetime, timedelta
from .container import create_container, create_container_readwrite_sas, create_container_readonly_sas, delete_container
from .model import generate_container_name, generate_task, is_expired
from .client import submit_request, check_processing

SUMMARY_QUERY = { '_id': 'summary' }

def default_configuration():
    return {
        "api_base_v4": environ['CAMELOT_DETECTOR_MEGADETECTOR_V4_API'],
        "caller": environ['CAMELOT_DETECTOR_MEGADETECTOR_CALLER'],
    }

# Matches default configuration in camelot-market
ANALYTICS_HIGH_CONFIDENCE_THRESHOLD = 0.9

def get_images_with_classifications(result_data):
    if "images" not in result_data:
        return set()
    images = result_data["images"]
    filenames = { x["file"] for x in images if "file" in x }
    return filenames

def elide_processing_response(task):
    task = dict(task)
    if 'processing_response' in task:
        del task['processing_response']
    return task

class Task:
    """Perform operations on tasks."""

    def __init__(self, Logger, Database, EventBus, Storage, TaskQueue, configuration=None):
        if not configuration:
            configuration = default_configuration()
        self.log = Logger
        self.event_bus = EventBus
        self.storage = Storage
        self.task_queue = TaskQueue
        self.caller = configuration['caller']
        self.api_base_v4 = configuration['api_base_v4']
        self.tasks = Database.get_tasks()

    def _create_container(self, sa_name):
        start = datetime.now().timestamp() * 1000
        try:
            keys = get_component('storage').get_account_keys(sa_name)
            sa_key = keys[0]
            container_name = generate_container_name()
            create_container(sa_name, sa_key, container_name)
            readwrite_sas = create_container_readwrite_sas(sa_name, sa_key, container_name)
            readonly_sas = create_container_readonly_sas(sa_name, sa_key, container_name)
            latency = round((datetime.now().timestamp() * 1000) - start)
            self.log.event({
                'state': 'ok' if latency < 10000 else 'warning',
                'service': 'detector azure container creation latency',
                'description': 'Create container succeeded',
                'metric': latency,
                'tags': ['task', 'creation', 'container', 'created']
            })
            return {
                'name': container_name,
                'readwrite_sas': readwrite_sas,
                'readonly_sas': readonly_sas
            }
        except Exception as e:
            latency = round((datetime.now().timestamp() * 1000) - start)
            self.log.event({
                'state': 'critical',
                'service': 'detector azure container creation latency',
                'description': 'Create container failed',
                'metric': latency,
                'tags': ['task', 'azure', 'creation', 'container', 'latency', 'failed']
            })
            raise e


    def _api_base(self, request_id):
        return self.api_base_v4

    def _check_processing(self, request_id):
        start = datetime.now().timestamp() * 1000
        try:
            resp = check_processing(self._api_base(request_id), request_id)
            request_latency = round((datetime.now().timestamp() * 1000) - start)
            self.log.event({
                'state': "ok" if request_latency < 10000 else "warning",
                'service': 'detector batch_processor GET job latency',
                'description': 'GET job succeeded',
                'metric': request_latency,
                'tags': ['task', 'batch_processor', 'get_request_id', 'latency']
            })
            return resp
        except Exception as e:
            latency = round((datetime.now().timestamp() * 1000) - start)
            self.log.event({
                'state': 'warning',
                'service': 'detector batch_processor GET job latency',
                'description': 'GET job failed',
                'metric': latency,
                'tags': ['task', 'batch_processor', 'get_request_id', 'failed']
            })
            raise e

    def _fetch_image_results(self, query, task):
        api_version = 'v4'
        if len(task['request_id']) <= 4:
            api_version = 'v2'
        try:
            if 'result' in task:
                return task
            processing_response = task['processing_response']
            if processing_response['failed_images']:
                failed_images = make_get_request(processing_response['failed_images'])
                self._record_failed_images(task, failed_images)
            self._record_task_completion()
            detections = make_get_request(processing_response["detections"])
            if processing_response["images"]:
                requested_images_for_classification_set = set(make_get_request(processing_response["images"]))
            else:
                requested_images_for_classification_set = None
            images_with_classifications_set = get_images_with_classifications(detections)
            if requested_images_for_classification_set and not requested_images_for_classification_set.issuperset(images_with_classifications_set):
                self._record_unexpected_images(images_with_classifications_set, requested_images_for_classification_set, api_version)
                old_status = task['status']
                task = self.tasks.find_one_and_update(
                    query,
                    {'$set': { 'status': 'FAILED', 'result': 'Unexpected results received' } },
                    return_document=ReturnDocument.AFTER
                )
                self._update_task_status_summaries(old_status, 'FAILED')
                # We've done all we can here.
                return task
            if "images" in detections:
                self._record_processed_images(detections)
            self.log.event({
                'state': 'ok',
                'service': 'detector batch_processor GET result_json count',
                'description': 'GET result_json failed',
                'metric': 1,
                'tags': ['task', 'batch_processor', 'get_result_json', 'latency']
            })
            self.log.event({
                'state': 'ok',
                'service': 'detector batch_processor GET result_json api_' + api_version + ' count',
                'description': 'GET result_json failed',
                'metric': 1,
                'tags': ['task', 'batch_processor', 'get_result_json', 'count']
            })
            return {
                **task,
                "result": detections,
            }
        except Exception as e:
            self.log.event({
                'state': 'warning',
                'service': 'detector batch_processor GET result_json failed count',
                'description': 'GET result_json failed',
                'metric': 1,
                'tags': ['task', 'batch_processor', 'get_result_json', 'failed', 'count']
            })
            self.log.event({
                'state': 'ok',
                'service': 'detector batch_processor GET result_json api_' + api_version + ' count',
                'description': 'GET result_json failed',
                'metric': 1,
                'tags': ['task', 'batch_processor', 'get_result_json', api_version, 'count']
            })
            raise e

    def _process_result(self, query, status, result):
        task = self.tasks.find_one(query)
        # Should only happen when getting back a totally different task ID.
        if not task:
            return None
        self._check_lost_job(task, result)
        if status == 'SUBMITTED':
            return self._handle_task_expiry(query, task)
        elif status == 'COMPLETED':
            self._record_processing_latency(task, result)
            return self._update_task_with_processing_response_data(query, status, result)
        else:
            self._record_processing_latency(task, result)
            resp_data = result["response_data"]
            return self._update_task_with_result_data(query, status, resp_data)

    def _get_processing(self, query):
        task = self.tasks.find_one(query)
        if task and task['status'] == 'SUBMITTED':
            if 'processing_response' not in task:
                (status, result) = self._check_processing(task['request_id'])
                if "task_id" in result and result["task_id"] != str(task["_id"]):
                    # We got a task back, but not the one we wanted!
                    query = { '_id': ObjectId(result["task_id"]), "status": "SUBMITTED" }
                    self._process_result(query, status, result)
                    self._record_unexpected_task_id(query, task, result)
                    # Tell client something has gone wrong, and that they should retry.
                    return {
                        **task,
                        'status': 'FAILED',
                        'result': {'error': 'Job ID collision detected'}
                    }
                else:
                    return self._process_result(query, status, result)
            else:
                self._record_no_activity()
                return task
        else:
            self._record_no_activity()
            return task

    def _get(self, query, skip_fetch):
        task = self._get_processing(query)
        if not skip_fetch and task and 'processing_response' in task:
            return self._fetch_image_results(query, task)
        return task

    def get(self, account, task_id, **kwargs):
        skip_fetch = kwargs.get('skip_fetch', False)
        query = {'_id': ObjectId(task_id), 'account_id': account['_id']}
        return elide_processing_response(self._get(query, skip_fetch))

    def get_task_nocheck(self, account, task_id):
        query = {'_id': ObjectId(task_id), 'account_id': account['_id']}
        return self.tasks.find_one(query)

    def is_expired(self, task):
        if 'container_expiry' not in task:
            return False
        return is_expired(task['container_expiry'])

    def get_all_non_archived(self):
        query = {"status": {"$ne": "ARCHIVED" }}
        return self.tasks.find(query)

    def _submit_task(self, task):
        start = datetime.now().timestamp() * 1000
        try:
            resp = submit_request(self.api_base_v4, self.caller, task)
            latency = round((datetime.now().timestamp() * 1000) - start)
            self.log.event({
                'state': 'ok' if latency < 10000 else 'warning',
                'service': 'detector batch_processor submit latency',
                'metric': latency,
                'tags': ['task', 'batch_processor', 'submit', 'latency']
            })

            if task['status'] == 'PENDING':
                self.log.event({
                    'state': 'ok',
                    'service': 'detector batch_processor submit status pending count',
                    'metric': 1,
                    'tags': ['task', 'batch_processor', 'submit', 'count']
                })
            elif task['status'] == 'FAILED':
                self.log.event({
                    'state': 'ok',
                    'service': 'detector batch_processor submit status failed count',
                    'metric': 1,
                    'tags': ['task', 'batch_processor', 'submit', 'count']
                })

            return resp
        except Exception as e:
            latency = round((datetime.now().timestamp() * 1000) - start)
            self.log.event({
                'state': "critical",
                'service': 'detector batch_processor submit latency',
                'description': 'Submit task failed',
                'metric': latency,
                'tags': ['task', 'batch_processor', 'submit', 'latency', 'failed']
            })
            raise e

    def submit(self, account, task_id):
        query = {
            '_id': ObjectId(task_id),
            'account_id': account['_id'],
            '$or': [{ 'status': 'PENDING' }, { 'status': 'FAILED' }, { 'status': 'SUBMITTED' }]
        }
        task = self.tasks.find_one(query)
        if not task:
            return None

        if task['status'] == 'SUBMITTED':
            return task

        if 'container' not in task:
            return task

        response = self._submit_task(task)
        old_status = task['status']
        task = self.tasks.find_one_and_update(
            query,
            {'$set': { 'status': 'SUBMITTED',
                       'request_id': response['request_id'],
                       'submission_timestamp': int(datetime.utcnow().timestamp() * 1000)} },
            return_document=ReturnDocument.AFTER
        )
        self._update_task_status_summaries(old_status, 'SUBMITTED')
        self.log.event({
            'state': 'ok',
            'description': 'task submitted count',
            'service': 'detector task submission count',
            'metric': 1,
            'tags': ['task', 'submission', 'task_submitted', 'count']
        })
        return task

    def create(self, account):
        start = datetime.now().timestamp() * 1000
        try:
            sa_name = account['storage_account_name']
            container = self._create_container(sa_name)
            task = generate_task(account, container)
            document = self.tasks.insert_one(task)
            self._update_task_status_summaries(None, task['status'])
            latency = round((datetime.now().timestamp() * 1000) - start)
            self.log.event({
                'state': 'ok' if latency < 10000 else 'warning',
                'service': 'detector task creation latency',
                'description': 'Create task succeeded',
                'metric': latency,
                'tags': ['task', 'creation', 'task_created', 'latency']
            })
            return task
        except Exception as e:
            latency = round((datetime.now().timestamp() * 1000) - start)
            self.log.event({
                'state': 'critical',
                'service': 'detector task creation latency',
                'description': 'Create task failed',
                'metric': latency,
                'tags': ['task', 'creation', 'failed', 'latency']
            })
            raise e

    def archive(self, account, task_id):
        start = datetime.now().timestamp() * 1000
        try:
            query = {
                '_id': ObjectId(task_id),
                'account_id': account['_id']
            }
            task = self.tasks.find_one(query)
            if not task:
                return None

            if not 'container' in task:
                return task

            sa_name = account['storage_account_name']
            keys = self.storage.get_account_keys(sa_name)
            sa_key = keys[0]

            try:
                delete_container(sa_name, sa_key, task['container']['name'])
            except Exception as e:
                self.log.warn("task", "Container could not be deleted: " + task['container']['name'] + ". Exception: " + str(e))
            self.tasks.find_one_and_update(
                query,
                {'$unset': { 'container': '',
                             'container_expiry': ''},
                 '$set': { 'status': 'ARCHIVED' } },
                return_document=ReturnDocument.AFTER
            )
            self._update_task_status_summaries(task['status'], 'ARCHIVED')
            latency = round((datetime.now().timestamp() * 1000) - start)
            self.log.event({
                'state': 'ok',
                'service': 'detector task archival latency',
                'description': 'Archive task succeeded',
                'metric': latency,
                'tags': ['task', 'archival', 'archived', 'latency']
            })
            return task
        except Exception as e:
            latency = round((datetime.now().timestamp() * 1000) - start)
            self.log.event({
                'state': 'critical',
                'service': 'detector task archival latency',
                'description': 'Archive task failed',
                'metric': latency,
                'tags': ['task', 'archival', 'failed', 'latency']
            })
            raise e

    def _record_processing_latency(self, task, result):
        if "end_time" in result and "start_time" in result:
            processing_latency = (result["end_time"] - result["start_time"]).total_seconds() * 1000
            self.log.event({
                'state': "ok" if processing_latency < (12 * 60 * 60 * 1000) else "warning",
                'service': 'detector batch_processor processing latency',
                'metric': processing_latency,
                'tags': ['task', 'processing', 'batch_processor', 'total_processing_time']
            })

    def _record_failed_images(self, task, failed_images):
        num_failed_images = len(failed_images)
        if num_failed_images > 0:
            self.log.warn("task", "task %s has %d failed images" % (str(task['_id']), num_failed_images))
            self.log.event({
                'state': 'warning',
                'service': 'detector batch_processor job failed images count',
                'description': 'number of failed images in the result set',
                'metric': num_failed_images,
                'tags': ['task', 'batch_processor', 'retrieval', 'failed_images', 'count']
            })

    def _record_task_completion(self):
        self.log.event({
            'state': 'ok',
            'service': 'detector batch_processor job completed event count',
            'description': 'new completion received',
            'metric': 1,
            'tags': ['task', 'batch_processor', 'retrieval', 'completed', 'count']
        })

    def _record_unexpected_images(self, images_with_classifications_set, requested_images_for_classification_set, api_version):
        self.log.warn("task", "Unexpected images found in result set. Found: %s but expected %s" % (str(images_with_classifications_set), str(requested_images_for_classification_set)))
        self.log.event({
            'state': 'warning',
            'service': 'detector batch_processor job unexpected images found count',
            'description': 'found unexpected images in the result set',
            'metric': 1,
            'tags': ['task', 'batch_processor', 'retrieval', 'collision', 'unexpected_images', 'count']
        })
        self.log.event({
            'state': 'warning',
            'service': 'detector batch_processor job unexpected images found api_' + api_version + ' count',
            'description': 'found unexpected images in the result set',
            'metric': 1,
            'tags': ['task', 'batch_processor', 'retrieval', 'collision', 'unexpected_images', api_version, 'count']
        })
        self.log.event({
            'state': 'warning',
            'service': 'detector batch_processor job unexpected images total count',
            'description': 'number of unexpected images in the result set',
            'metric': len(images_with_classifications_set.difference(requested_images_for_classification_set)),
            'tags': ['task', 'batch_processor', 'retrieval', 'collision', 'unexpected_images', 'count']
        })

    def _record_processed_images(self, resp_data):
        total_images = len(resp_data["images"])
        self.log.event({
            'state': 'ok',
            'service': 'detector batch_processor images processed count',
            'metric': total_images,
            'tags': ['batch_processor', 'processing', 'task', 'total_images', 'count']
        })

        high_confidence_images = [x for x in resp_data["images"] if 'max_detection_conf' in x and x['max_detection_conf'] > ANALYTICS_HIGH_CONFIDENCE_THRESHOLD]
        high_confidence_count = len(high_confidence_images)
        self.log.event({
            'state': 'ok',
            'service': 'detector batch_processor images confident count',
            'metric': high_confidence_count,
            'tags': ['batch_processor', 'processing', 'task', 'confident_images', 'count']
        })

        if total_images > 0:
            self.log.event({
                'state': 'ok',
                'service': 'detector batch_processor images confident percentage',
                'metric': high_confidence_count / total_images,
                'tags': ['batch_processor', 'processing', 'task', 'confident_percentage']
            })

    def _update_task_with_result_data(self, query, status, resp_data):
        old_status = self.tasks.find_one(query)['status']
        task = self.tasks.find_one_and_update(
            query,
            {'$set': { 'status': status, 'result': resp_data } },
            return_document=ReturnDocument.AFTER
        )
        self._update_task_status_summaries(old_status, status)
        self.log.event({
            'state': 'ok',
            'service': 'detector task status update count',
            'metric': 1,
            'tags': ['task', 'retrieval', 'status_updated']
        })
        return task

    def _update_task_with_processing_response_data(self, query, status, resp_data):
        if 'start_time' in resp_data and 'end_time' in resp_data:
            serialised_response = {
                **resp_data,
                'start_time': resp_data["start_time"].timestamp(),
                'end_time': resp_data["end_time"].timestamp()
            }
        else:
            serialised_response = resp_data
        old_status = self.tasks.find_one(query)['status']
        task = self.tasks.find_one_and_update(
            query,
            {'$set': { 'status': status, 'processing_response': serialised_response } },
            return_document=ReturnDocument.AFTER
        )
        self._update_task_status_summaries(old_status, status)
        self.log.event({
            'state': 'ok',
            'service': 'detector task processing_response update count',
            'metric': 1,
            'tags': ['task', 'retrieval', 'processing_response_updated']
        })
        return task

    def publish_metrics(self):
        summary = self.tasks.find_one(SUMMARY_QUERY)
        for key in summary.keys():
            if key != '_id':
                self.log.event({
                    'state': 'ok',
                    'service': 'detector task summary ' + key + ' count',
                    'description': 'number of ' + key + ' tasks',
                    'metric': summary[key],
                    'tags': ['task', 'task', 'summary', key, 'count']
                })

    def _record_unexpected_task_id(self, query, task, result):
        self.log.warn("task", "Received task %s but expected task %s at %s" % (result["task_id"], task["_id"], task['request_id']))
        self.log.event({
            'state': 'warning',
            'service': 'detector batch_processor job collision count',
            'description': 'unexpected task ID received',
            'metric': 1,
            'tags': ['task', 'batch_processor', 'retrieval', 'status_updated', 'unexpected_task_id', 'count']
        })

    def _handle_task_expiry(self, query, task):
        # Kludge to get around the fact that we may not observe the
        # completion of a job, particularly due to request_id collisions
        submission_expiry_datetime = (datetime.utcnow() - timedelta(days=4))
        if 'submission_timestamp' in task and task['submission_timestamp'] < (submission_expiry_datetime.timestamp() * 1000):
            old_status = task['status']
            task = self.tasks.find_one_and_update(
                query,
                {'$set': { 'status': 'FAILED', 'result': 'Submission expired' } },
                return_document=ReturnDocument.AFTER
            )
            self._update_task_status_summaries(old_status, 'FAILED')
            self.log.event({
                'state': 'warning',
                'service': 'detector task submission expired count',
                'metric': 1,
                'tags': ['task', 'retrieval', 'status_updated', 'submission_expired', 'count']
            })
            self.log.warn("task", "Submission expiry reached. Marking as FAILED: %s" % (task['_id']))
        return task

    def _update_task_status_summaries(self, old_status, new_status):
        if old_status:
            self._increment_status_summary(old_status, -1)
        self._increment_status_summary(new_status, 1)

    def _increment_status_summary(self, status, amount):
        self.tasks.find_one_and_update(
            SUMMARY_QUERY,
            { '$inc': { status: amount }}
        )

    def _record_no_activity(self):
        self.log.event({
            'state': 'ok',
            'service': 'detector task check',
            'description': 'task checked - no activity',
            'tags': ['task', 'retrieval', 'check_complete', 'no_activity']
        })

    def _check_lost_job(self, task, result):
        if 'job_lost' in result and result['job_lost'] == True:
            self.log.warn("task", "Job lost for task: " + str(task['_id']))
            self.log.event({
                'state': "warning",
                'service': 'detector batch_processor job lost count',
                'description': 'batch_processor job lost',
                'metric': 1,
                'tags': ['task', 'processing', 'batch_processor', 'job_lost', 'count']
            })
