"""Handle task queue messages."""

import os
import json
import asyncio
import random
import threading
import traceback

from azure.storage.queue import (QueueClient,TextBase64EncodePolicy,TextBase64DecodePolicy)

def default_configuration():
    return {
        'storage_account': os.getenv('CAMELOT_DETECTOR_STORAGE_ACCOUNT'),
        'queue_name': os.getenv('CAMELOT_DETECTOR_TASK_QUEUE_NAME'),
    }

RETRY_LIMIT = 20
DEFAULT_MAX_BACKOFF = 15
DEFAULT_SCALING_FACTOR = 1.4
DEFAULT_INITIAL_BACKOFF = 1
DEFAULT_JITTER = 0.1

def next_backoff(current_backoff, **kwargs):
    max_backoff = kwargs.get('max_backoff', DEFAULT_MAX_BACKOFF)
    scaling_factor = kwargs.get('scaling_factor', DEFAULT_SCALING_FACTOR)
    base_jitter = kwargs.get('jitter', DEFAULT_JITTER)
    jitter = random.random() * base_jitter
    base_value = min(max_backoff, current_backoff * scaling_factor)
    return base_value - (base_jitter*0.5) + jitter

class EmptyQueueException(Exception):
    pass

class TaskQueue:
    """Produce and consume worker messages."""

    def __init__(self, Logger, configuration=None, Client=QueueClient):
        self.log = Logger
        if not configuration:
            configuration = default_configuration()
        self.client = QueueClient.from_connection_string(
            configuration.get('storage_account'),
            configuration.get('queue_name'),
            message_encode_policy = configuration.get('message_encode_policy', TextBase64EncodePolicy()),
            message_decode_policy = configuration.get('message_decode_policy', TextBase64DecodePolicy()))

    async def _with_retry_policy(self, callback, **kwargs):
        """Callback and retry policy arguments."""
        initial_backoff = kwargs.get('initial_backoff', DEFAULT_INITIAL_BACKOFF)
        current_backoff = initial_backoff
        while True:
            try:
                result = callback()
                current_backoff = initial_backoff
            except Exception as e:
                err_msg = str(e)
                if err_msg != "Empty queue":
                    self.log.warn("TaskQueue", ("Task processing failed due to exception: %s" % err_msg))
                await asyncio.sleep(current_backoff)
                current_backoff = next_backoff(current_backoff, **kwargs)

    def get_or_create_eventloop(self):
        try:
            return asyncio.get_event_loop()
        except RuntimeError as ex:
            if "There is no current event loop in thread" in str(ex):
                loop = asyncio.new_event_loop()
                asyncio.set_event_loop(loop)
                return asyncio.get_event_loop()

    def _start_async_consumer(self):
        loop = self.get_or_create_eventloop()
        loop.run_until_complete(self._with_retry_policy(lambda: self.consume()))

    def produce(self, message, visibility_timeout=0):
        self.client.send_message(
            json.dumps(message),
            visibility_timeout=visibility_timeout)
        self.log.event({
            'state': 'ok',
            'service': 'detector task_queue message produced count',
            'metric': 1,
            'tags': ['task_queue', 'message', 'produced', 'count']
        })

    def start_consumer(self, consumer):
        self.consumer = consumer
        thread = threading.Thread(target=self._start_async_consumer)
        thread.start()
        self.log.event({
            'state': 'ok',
            'service': 'detector task_queue consumer started count',
            'metric': 1,
            'tags': ['task_queue', 'consumer', 'started', 'count']
        })

    def consume(self):
        message = self.client.receive_message()
        if message is None:
            raise EmptyQueueException("Empty queue")

        self.consumer(json.loads(message.content))
        self.client.delete_message(message.id, message.pop_receipt)
        self.log.event({
            'state': 'ok',
            'service': 'detector task_queue message consumed count',
            'metric': 1,
            'tags': ['task_queue', 'message', 'consumed', 'count']
        })
