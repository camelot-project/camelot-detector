"""Record learning results"""

THRESHOLDS = (0.1, 0.3, 0.50, 0.75, 0.85, 0.90, 0.95, 0.99, 0.995)

def display_threshold(threshold):
    if threshold == 0.995:
        return "99_5"
    else:
        return ("%.2f" % (threshold))[2:]

class Learn:
    """Perform learning on results."""

    def __init__(self, Logger):
        self.log = Logger

    def learn_bulk(self, account, results):
        if len(results) == 0:
            return

        self.log.event({
            'state': 'ok',
            'service': 'detector learn bulk total_result count',
            'description': 'Count of the total results processed for learning',
            'metric': len(results),
            'tags': ['detector', 'learn', 'bulk', 'total_results', 'count']
        })

        for threshold in THRESHOLDS:
            false_positives = 0
            true_negatives = 0
            true_positives = 0
            false_negatives = 0

            for result in results:
                if result['has_animals_actual'] == True:
                    if result['has_animals_detection_max_confidence'] >= threshold:
                        true_positives += 1
                    else:
                        false_negatives += 1
                else:
                    if result['has_animals_detection_max_confidence'] >= threshold:
                        false_positives += 1
                    else:
                        true_negatives += 1

            self.log.event({
                'state': 'ok',
                'service': 'detector learn bulk true_positive_' + display_threshold(threshold) + ' count',
                'description': 'Count of the results that were true positives',
                'metric': true_positives,
                'tags': ['detector', 'learn', 'bulk', 'true_positive', 'count', 'threshold_'+ display_threshold(threshold)]
            })

            self.log.event({
                'state': 'ok',
                'service': 'detector learn bulk false_negative_' + display_threshold(threshold) + ' count',
                'description': 'Count of the results that were false negatives',
                'metric': false_negatives,
                'tags': ['detector', 'learn', 'bulk', 'false_negative', 'count', 'threshold_'+ display_threshold(threshold)]
            })

            self.log.event({
                'state': 'ok',
                'service': 'detector learn bulk false_positive_' + display_threshold(threshold) + ' count',
                'description': 'Count of the results that were false positives',
                'metric': false_positives,
                'tags': ['detector', 'learn', 'bulk', 'false_positive', 'count', 'threshold_'+ display_threshold(threshold)]
            })

            self.log.event({
                'state': 'ok',
                'service': 'detector learn bulk true_negative_' + display_threshold(threshold) + ' count',
                'description': 'Count of the results that were true negatives',
                'metric': true_negatives,
                'tags': ['detector', 'learn', 'bulk', 'true_negative', 'count', 'threshold_'+ display_threshold(threshold)]
            })
