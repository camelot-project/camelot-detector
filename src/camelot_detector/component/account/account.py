"""Account Management"""

from os import environ
from .model import generate_account, generate_apikey

def to_account(account):
    """Return only profile information for the account."""
    return { key: account[key] for key in ['_id', 'email_address', 'storage_account_name'] }

def default_configuration():
    return {
        "common_storage_account": environ['CAMELOT_DETECTOR_COMMON_STORAGE_ACCOUNT']
    }

class Account:
    """Perform operations on accounts."""

    def __init__(self, Logger, Database, SendMail, configuration=None):
        if not configuration:
            configuration = default_configuration()
        self.storage_account = configuration['common_storage_account']
        self.log = Logger
        self.accounts = Database.get_accounts()
        self.sendmail = SendMail

    def register(self, email_address):
        """Register a new user account for `email_address`.
        Returns `None` if the user account already exists."""
        existing_account = self._get_account_by_email(email_address)
        if existing_account:
            self.log.warn("Account", "Attempt to re-register an email address owned by user %s" % existing_account['_id'])
            self.log.event({
                'state': 'warning',
                'service': 'detector account registration already exists count',
                'metric': 1,
                'tags': ['account', 'registration', 'already_exists', 'count']
            })
            return None

        account = generate_account(email_address, self.storage_account)
        self._create_account(account)
        self._send_registration_email(account)

        return account

    def authenticate(self, email_address, apikey):
        """Returns the account, or None if it cannot be validated."""
        account = self._get_account_by_email(email_address)
        if account and account['apikey'] == apikey:
            self.log.event({
                'state': 'ok',
                'service': 'detector account authentication succeeded count',
                'metric': 1,
                'tags': ['account', 'authentication', 'authenticated', 'count']
            })
            return to_account(account)
        else:
            self.log.event({
                'state': 'warning',
                'service': 'detector account authentication failed count',
                'metric': 1,
                'tags': ['account', 'authentication', 'incorrect_api_key', 'count']
            })
            return None

    def get_account(self, account_id):
        """Retrieve account data for the account with the given ID.
        Returns None if it cannot be found."""
        account = self.accounts.find_one({'_id': account_id})
        if not account:
            return None
        else:
            return to_account(account)

    def _send_registration_email(self, account):
        self.sendmail.send(
            account['email_address'],
            'Camelot Project registration',
            """Welcome to the Camelot Project.
            <p>
            Your API key is: %s
            </p>""" % account['apikey']
        )
        self.log.event({
            'state': 'ok',
            'service': 'detector account registration email sent count',
            'metric': 1,
            'tags': ['account', 'registration', 'email_sent', 'count']
        })

    def _get_account_by_email(self, email_address):
        """Return true if an account for the given email already exists.
        False otherwise."""
        return self.accounts.find_one({'email_address': email_address})

    def _create_account(self, account):
        """Insert the given account into the collection."""
        account = self.accounts.insert_one(account)
        self.log.event({
            'state': 'ok',
            'service': 'detector account registration created count',
            'metric': 1,
            'tags': ['account', 'registration', 'created', 'count']
        })
        return account
