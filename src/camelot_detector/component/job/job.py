"""Job management"""

from pymongo import ReturnDocument
from datetime import datetime, timedelta
import camelot_detector.shared.constants as constants

def calculate_expiry(dt=None):
    if not dt:
        dt = datetime.utcnow()
    expiry_dt = dt + constants.JOB_OWNERSHIP_GRACE_PERIOD
    return int(expiry_dt.timestamp() * 1000)

def is_valid(prev_job):
    expiry = prev_job['expiry']
    if 'expiry' not in prev_job:
        return False
    dt = int(datetime.utcnow().timestamp() * 1000)
    return dt <= expiry

class Job:
    """Perform operations on tasks."""

    def __init__(self, Logger, Database, EventBus):
        self.log = Logger
        self.jobs = Database.get_jobs()
        self.event_bus = EventBus
        self.event_bus.subscribe("task_processed", lambda event: self.remove(event["request_id"]))
        self.event_bus.subscribe("task_submitted", lambda event: self.add(event["request_id"], event["task"]))
        self.collisions = 0
        self.total = 0

    def add(self, job_id, task):
        query = {
            '_id': job_id,
        }
        task_id = task['_id']
        prev_job = self.jobs.find_one_and_update(
            query,
            {'$set': { 'task_id': task_id, 'expiry': calculate_expiry() } },
            return_document=ReturnDocument.BEFORE,
            upsert=True
        )

        if self.total >= 100:
            if self.collisions > 0:
                self.log.warn("job", "Detected collisions in the last events: %d / %d" % (self.collisions, self.total))
            self.total = 0
            self.collisions = 0

        self.total += 1

        # Check & handle collisions
        if not prev_job:
            return
        prev_task_id = prev_job["task_id"]
        if prev_task_id != task_id:
            if is_valid(prev_job):
                self.collisions += 1
                self.log.warn("job", "Collision detected for %s (task was %s, now %s)" % (job_id, prev_job["task_id"], task['_id']))
                self.event_bus.publish({
                    'type': 'job_collision',
                    'task_id': prev_task_id,
                });
            else:
                self.log.warn("job", "Collision detected on expired task for job_id %s (task was %s, now %s)" % (job_id, prev_job["task_id"], task['_id']))
                self.event_bus.publish({
                    'type': 'job_invalidated',
                    'task_id': prev_task_id,
                });

    def remove(self, request_id):
        query = { '_id': request_id }
        self.jobs.delete_one(query)

