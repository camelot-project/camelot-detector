"""Handling for detector tasks"""

from camelot_detector.shared.system import get_component
from camelot_detector.shared.http_utils import from_bson

def task_create(*args, **kwargs):
    """Create a new task"""
    account_id = kwargs['user']
    account = get_component('account').get_account(account_id)
    if not account:
        # This would be bad, as we have made it past authn
        return None, 401
    task = get_component('task').create(account)
    return from_bson(task), 201

def task_get(*args, **kwargs):
    """Retrieve a given task"""
    task_id = kwargs['id']
    account_id = kwargs['user']
    account = get_component('account').get_account(account_id)
    if not account:
        # This would be bad, as we have made it past authn
        return None, 401
    task = get_component('task').get(account, task_id)
    return from_bson(task), 200

def task_submit(*args, **kwargs):
    """Submit a task for processing"""
    task_id = kwargs['id']
    account_id = kwargs['user']
    account = get_component('account').get_account(account_id)
    if not account:
        # This would be bad, as we have made it past authn
        return None, 401
    task = get_component('task').submit(account, task_id)
    if not task:
        return None, 404
    if 'container' not in task:
        return None, 400
    return from_bson(task), 200

def task_archive(*args, **kwargs):
    """Archive the given task"""
    task_id = kwargs['id']
    account_id = kwargs['user']
    account = get_component('account').get_account(account_id)
    if not account:
        # This would be bad, as we have made it past authn
        return None, 401
    task = get_component('task').archive(account, task_id)
    if task:
        return from_bson(task), 200
    else:
        return 'Task not found', 404
