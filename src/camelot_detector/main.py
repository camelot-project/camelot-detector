"""Initialisation module"""

import os
import atexit
from camelot_detector.api import APP
from camelot_detector.core.system import init_system
from camelot_detector.shared.system import get_component, is_worker
from camelot_detector.component.message_processor import MessageProcessor
from applicationinsights.flask.ext import AppInsights
from datetime import datetime, timedelta
from flask import request, got_request_exception

KNOWN_TOP_LEVEL_PATHS = set(['healthcheck', 'account', 'task', 'learn', 'maintenance'])

def track_client_version():
    version = request.headers.get('x-camelot-version')
    if version:
        get_component('log').event({
            'state': 'ok',
            'service': 'detector API client camelot version ' + version.replace('.', '_').replace('-', '_') + ' count',
            'metric': 1,
            'tags': ['detector', 'api', 'client', 'camelot', 'version', 'count']
        })
    else:
        get_component('log').event({
            'state': 'ok',
            'service': "detector API client other count",
            'metric': 1,
            'tags': ['detector', 'api', 'client', 'other', 'count']
        })

def before_request_handler():
    request.processing_start_time = datetime.now().timestamp() * 1000
    track_client_version()

def parameterise(param):
    # Poor proxy for objectId
    if len(param) == 24:
        return 'OBJECT_ID'
    else:
        return param

def classify_status_state(status_code):
    if status_code < 400:
        return 'ok'
    else:
        return 'warning'

def after_request_handler(response):
    elapsed_time = round(datetime.now().timestamp() * 1000 - request.processing_start_time)
    path_params = [parameterise(x) for x in request.path.split('/') if x != '']

    if len(path_params) == 0 or path_params[0] not in KNOWN_TOP_LEVEL_PATHS:
        get_component('log').warn("http", "Requested path unknown: %s" % (request.path))
        get_component('log').event({
            'state': 'warning',
            'service': "detector API url invalid latency",
            'metric': elapsed_time,
            'tags': ['detector', 'api', 'latency', 'url_invalid']
        })
        return response

    service_suffix = ' '.join(path_params)
    state = classify_status_state(response.status_code)
    get_component('log').event({
        'state': state,
        'service': "detector API %s %s latency" % (request.method, service_suffix),
        'metric': elapsed_time,
        'tags': ['detector', 'api', 'latency'] if state == 'ok' else ['detector', 'api', 'latency', 'failed']
    })
    return response

def exception_handler(sender, exception, **extra):
    elapsed_time = round(datetime.now().timestamp() * 1000 - request.processing_start_time)
    service_suffix = ' '.join([parameterise(x) for x in request.path.split('/') if x != ''])
    get_component('log').event({
        'state': 'warning',
        'service': "detector API %s %s latency" % (request.method, service_suffix),
        'metric': elapsed_time,
        'tags': ['detector', 'api', 'latency', 'failed']
    })

if is_worker():
    init_system(None)
    task_queue = get_component('task_queue')
    task = get_component('task')
    account = get_component('account')
    logger = get_component('log')
    message_processor = MessageProcessor(Logger=logger, Task=task, Account=account)
    task_queue.start_consumer(message_processor.handle)

    logger.event({
        'state': 'ok',
        'service': "detector worker process started count",
        'metric': 1,
        'tags': ['detector', 'system', 'lifecycle', 'startup']
    })

else:
    app = APP
    app.app.config['APPINSIGHTS_INSTRUMENTATIONKEY'] = os.getenv('CAMELOT_DETECTOR_APPINSIGHTS_KEY')
    appinsights = AppInsights(app.app)
    app.app.before_request(before_request_handler)
    app.app.after_request(after_request_handler)
    got_request_exception.connect(exception_handler, app.app)
    init_system(app.app)
    get_component('log').event({
        'state': 'ok',
        'service': "detector web process started count",
        'metric': 1,
        'tags': ['detector', 'system', 'lifecycle', 'startup']
    })

def process_exit_handler():
    get_component('log').event({
        'state': 'ok',
        'service': "detector process stopped",
        'metric': 1,
        'tags': ['detector', 'system', 'lifecycle', 'startup']
    })

atexit.register(process_exit_handler)
